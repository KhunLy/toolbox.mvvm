﻿using System;
using System.Windows.Input;

namespace ToolBox.MVVM.Commands
{
    public class CommandBase : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private Action action;

        public CommandBase(Action action)
        {
            if (action is null) throw new ArgumentException();
            this.action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            action?.Invoke();
        }
    }
}

